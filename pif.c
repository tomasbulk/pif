#include <unistd.h>			/* access(), F_OK */
#include <stdio.h>			/* printf(), snprintf(), popen(), pclose() */
#include <stdlib.h>			/* EXIT_SUCCESS, EXIT_FAILURE, malloc(), free() */
#include <stdarg.h>			/* va_start(), va_end(), type va_arg */
#include <string.h>			/* strlen() */
#include <limits.h>			/* PATH_MAX */
#include <libgen.h>			/* basename() */


struct DIR_PATH
{
	char *starting_dir;
	char *search_query;
	char *to_move;
};

struct RESULTS
{
	char **result;
	int length;
	int choice;
};

/* start of functions */

static size_t add_sizeof(int i, ...)
{
	size_t s 	= 0;

	va_list strings;
	va_start(strings, i);

	for (int j = 0; j < i; j++)
		s += strlen(va_arg (strings, char*));

	va_end(strings);

	return(s);
}


static void search_command(char **c, struct DIR_PATH D)
{
	size_t s;

	s 	= (add_sizeof(3, "find  -type d 2>/dev/null | grep -i ", D.starting_dir, D.search_query) + sizeof(char));
	*c 	= malloc(s);

	snprintf(*c, s, "find %s -type d 2>/dev/null | grep -i %s", D.starting_dir, D.search_query);
}


static void move_command(char **c, struct DIR_PATH D, struct RESULTS R)
{
	size_t s;

	s 	= add_sizeof(4, "mv \"\"\"\"  /", D.to_move, R.result[R.choice], basename(D.to_move));
	*c 	= malloc(s);

	snprintf(*c, s, "mv \"%s\" \"%s/%s\"", D.to_move, R.result[R.choice], basename(D.to_move));
}


static void fill_list(char ***list, FILE *stream, int *len)
{
	char *buff	= malloc (sizeof(char) * PATH_MAX);
	int index 	= 0;

	*list = malloc(sizeof(char*));
	while ( !feof(stream) )
	{
		fgets(buff, PATH_MAX, stream);
		buff[strlen(buff)-1] = '\0';

		*list 		= realloc(*list, sizeof(char*) * (size_t)(index + 1) );
		(*list)[index] 	= malloc(sizeof(char) * (strlen(buff) + 1));

		strcpy ((*list)[index], buff);

		index++;
	}

	free(buff);

	*len 		= index - 1;
	if ( !(*len) )
	{
		fprintf(stderr, "No results found.\n");
		exit(1);
	}
}


static void usage (char* argv0)
{
	fprintf(stderr, "Usage: %s [file] [search-query] [search-dir]\n\n", argv0);
	exit(1);
}


static void init_dir_path(struct DIR_PATH *D, char* argv[])
{
	D->to_move 	= argv[1];
	D->search_query = argv[2];
	D->starting_dir = argv[3] == NULL ? "~/" : argv[3];
}


static void get_results(struct DIR_PATH D, struct RESULTS *R)
{
	char *command;
	char **pl;
	int length;
	FILE *c_stream;

	search_command(&command, D);
	c_stream 	= popen(command, "r");
	free(command);

	fill_list(&pl, c_stream, &length);
	pclose(c_stream);

	R->result 	= pl;
	R->length 	= length;
}

static void print_results(struct RESULTS R)
{
	for ( int i = 0; i < R.length; i++ )
		printf("[%d] : %s\n", i, R.result[i]);
}
	

static void get_choice(struct RESULTS *R)
{
	scanf("%d", &R->choice);
	while(  R->choice < 0 ||  R->choice > R->length )
	{
		printf("Please enter a valid directory: ") ;
		scanf("%d", &R->choice);
	}
}

static int move_file(struct DIR_PATH D, struct RESULTS R)
{
	char *command;
	move_command(&command, D, R);

	printf("Running : %s\n", command);
	if (system(command) == -1)
	{
		fprintf(stderr, "Sorry, the transfer failed.\n");
		free(command);
		return(1);
	} else 
	{
		printf("File moved successfully.\n");
		free(command);
		return(0);
	}
}


/* end of functions */


int main (int argc, char *argv[])
{
	if (argc < 3)				/* display usage if not enough parameters */
		usage (argv[0]);

	if (access(argv[1], F_OK))		/* check for file reachability */
	{
		fprintf(stderr, "The file %s is not reachable.\n", argv[1]);
		exit(1);
	}

	struct DIR_PATH DIR_PATH;
	init_dir_path(&DIR_PATH, argv); 	/* fill DIR_PATH with .to_move .search_query .starting_dir */

	struct RESULTS RESULTS;
	get_results(DIR_PATH, &RESULTS);	/* fill RESULTS with .length .choice */

	print_results(RESULTS);			/* display all results */

	printf("Where would you like to place '%s'? (0-%d) : ", basename(DIR_PATH.to_move), RESULTS.length-1); 
	get_choice(&RESULTS);			/* record which directory to move the file to */

	if (move_file(DIR_PATH, RESULTS))	/* move the file, return an error if it doesn't succeed */
		return (1);
	
	return(0);				/* exit normally */
}
