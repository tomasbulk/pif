# set the compiler to gcc:
CC = gcc

# set the compiler flags
CFLAGS = -ggdb -Wall -Wextra -Wconversion -std=gnu11 -I.

# install location
PREFIX = /usr/local

SOURCES = pif.c
OBJECTS = $(SOURCES:.c=.o)
TARGET = pif

$(TARGET) : $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^

clean :
	rm -f $(TARGET) $(OBJECTS) core

install : $(TARGET)
	install -Dm755 $(PREFIX)/bin/$(TARGET)
uninstall :
	rm -rf $(PREFIX)/bin/pif

.PHONY : clean install uninstall
