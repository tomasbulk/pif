# pif

pif is a tool for putting files in folders (that you search for) 

## Installation

```
git clone https://gitlab.com/tomasbulk/pif.git
cd pif
make
make install
```

## Usage

```
pif -h

	PIF: A tool for putting-in-folders by searching for the folder.

	Usage: pif [file] [search-query] [opt: search-dir]


```
## Example
```
% pif random_file foo

  Result [1]:  /home/user/foo_dir

Please choose which directory to put 'random_file' in: 1

  Running `mv random_file /home/user/foo_dir/random_file`

  File moved succesfully.

% ls /home/user/foo_dir
random_file
%
```

## License
[GNU GPL v3](https://choosealicense.com/licenses/gpl-3.0/)
